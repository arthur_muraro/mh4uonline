# MH4UOnline

> by cloning this repo, you'll miss some files in cci_extract that were too big to push

> Make sure to get a proper ROM and do the entire procedure if you want to do some disassemble

> If anything misses, lemme know :) (I did this markdown asap so it's in draft state)

## resources

- https://www.retroreversing.com/3DSFileFormats
- https://github.com/archshift/ctr-elf
- https://bmaupin.github.io/wiki/other/3ds/3ds-tools.html
- https://github.com/dnasdw/3dstool

## making the .3ds file into something readable

extracting the .3ds file.

```
./tools/3dstool/bin/Release/3dstool -xvt017f cci cci_extract/0.cxi cci_extract/1.cfa cci_extract/7.cfa "sources/Monster Hunter 4 Ultimate (Europe) (En,Fr,De,Es,It).3ds" --header cci_extract/ncsdheader.bin
```

extracting the .cxi file.

```
./tools/3dstool/bin/Release/3dstool -xvtf cxi cci_extract/0.cxi --header cci_extract/ncchheader.bin --exh cci_extract/exh.bin --logo cci_extract/logo.bcma.lz --plain cci_extract/plain.bin --exefs cci_extract/exefs.bin --romfs cci_extract/romfs.bin
```

extracting the code the executable file system.

```
./tools/3dstool/bin/Release/3dstool -xtf exefs cci_extract/exefs.bin --exefs-dir cci_extract/exefs/
```

converting the binary into ELF file format, so it's easier to reverse.

```
python3 tools/ctr-elf2_custom/make_elf.py cci_extract
```